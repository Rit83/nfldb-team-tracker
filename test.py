import code

import nfldb

db = nfldb.connect()

def get_game(week):
    q = nfldb.Query(db)
    return q.game(team='DET', season_year=2015, season_type='Regular', week=week)

def get_drives(game):
    return game.as_drives()

def get_plays(week):
    plays = []
    for drive in get_drives(get_game(week)):
	if (drive.pos_team == 'DET'):
            for play in drive.plays:
                plays.append(play)
    return plays


def print_play(play):
    print play.description
    print "Time", play.time
    print "Down", play.down
    print "Distance", play.yards_to_go
    print "Passing Att:", play.passing_att
    print "Passing Cmp", play.passing_cmp
    print "Rushing Att", play.rushing_att

print """

query = q


get_plays(week)

ps = week 1 plays

"""

ps = get_plays(1)

code.interact(local=locals())
